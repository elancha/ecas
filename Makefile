DEBUG=1

CFLAGS+=-Wall -Wextra -std=c99 -pedantic -Wmissing-prototypes
CFLAGS+=-Wstrict-prototypes -Wold-style-definition

LDFLAGS+=

ifdef DEBUG
	CFLAGS+=-O0 -g
else
	CFLAGS+=-O3
endif

icweb: icweb.c
	$(CC) $(CFLAGS) -o icweb icweb.c $(LDFLAGS)
