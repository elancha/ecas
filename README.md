# ECAS

This is the repository for the ECAS computer algebra system and programming
language.

## Description

## Installation
To generate the executable just run
```terminal
$ gcc -o build build.c
$ ./build
```
NOTE: you can use any `C` compiler of your choice, simply change `gcc` by the
name of your compiler

## Usage


## Roadmap

## Contributing
Just drop a pull request no fancy requirements are needed.

## Authors and acknowledgment
Here's a list of people that have contributed to the project in some form
sorted in alphabetical order:
- Ernesto Lanchares [elancha98@gmail.com](mailto:elancha98@gmail.com)

## License

Copyright 2023, Ernesto Lanchares
[elancha98@gmail.com](mailto:elancha98@gmail.com).

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
