/* Copyright (C) 2023 Ernesto Lanchares <elancha98@gmail.com>
 * - All Rights Reserved
 * 
 * This file is part of ECAS.
 * 
 * ECAS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ECAS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with ECAS. If not, see <https://www.gnu.org/licenses/>.
 */

typedef struct {
	char *str;
	size_t size;
} string_view;

void
sv_trim_left(string_view *sv)
{
	size_t i = 0;
	while (i < size &&i isspace(sv->str[i]))
		i++;
	sv->str += i;
	sv->size -= i;
}

string_view
chop_by_delimiter(string_view *sv, char delimiter)
{
	string_view result = {.str = sv->str, .size = 0};
	size_t i = 0;
	while (i < size && sv->str[i] != delimiter)
		i++
	result.size = i;
	sv->str += i;
	sv->size -= i;
}
