/* Copyright (C) 2023 Ernesto Lanchares <elancha98@gmail.com>
 * - All Rights Reserved
 * 
 * This file is part of ECAS.
 * 
 * ECAS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ECAS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */


 /* This script is not part of the source code of ECAS. This is just the build
  * script used to compile the program. To compile ECAS run
  * $ gcc -o build build.c
  * $ ./build
  * And the executable `ecas` will be generated.
  */


#include <stdio.h>
#include <stdlib.h>

#define LOG_LEVEL 0

#define _XOPEN_SOURCE 700

char *CFLAGS[] = {
	"-Wall", "-Wextra", "-std=c99", "-pedantic", "-Wmissing-prototypes",
	"-Wstrict-prototypes", "-Wold-style-definition"
};
char *LDFLAGS[] = {"-lm"};

char *DEBUG_FLAGS[] = {"-O0", "-g"};
char *PROD_FLAGS[] = {"-O3"};

typedef struct {
	char **args;
	size_t count;
} Cmd;

static void
concat(Cmd *a, char *b[], size_t lenb)
{
	char **newa = malloc(sizeof(char *) * (a->count + lenb));
	for (size_t i = 0; i < a->count; i++) {
		newa[i] = a->args[i];
	}
	for (size_t i = 0; i < lenb; i++) {
		newa[i + a->count] = b[i];
	}
	a->count = a->count + lenb;
	a->args = newa;
}

#define BINARY_SOURCE(c, bin, src) \
	concat(c, (char *[]){"-o", bin, src}, 3)

#if _WIN32
	#if defined(__GNUC__)
		#define COMPILE_CMD "gcc"
	#elif define(__clang__)
		#define COMPILE_CMD "clang"
	#elif defined(_MSC_VER)
		#define COMPILE_CMD "cl.exe"
		#undef BINARY_SOURCE
		#define BINARY_SOURCE(c, bin, src) \
			concat(c, (char *[]){src}, 1)
	#endif
#else
	#define COMPILE_CMD "cc"
#endif

char *compile_cmd = COMPILE_CMD;
#define CONCAT(c, l) concat(c, l, sizeof(l)/sizeof(l[0]))

static void
INFO(Cmd *cmd)
{
#if LOG_LEVEL <= 0
	printf("[INFO]: Executing command \n");
	printf("$ ");
	for (size_t i = 0; i < cmd->count; i++) {
		printf("%s ", cmd->args[i]);
	}
	printf("\n");
#endif
}

static int
execute(Cmd *cmd)
{
	INFO(cmd);
}

int
main(int argc, char *argv[])
{
	Cmd mn = {.args = &compile_cmd, .count = 1};
	CONCAT(&mn, CFLAGS);
	CONCAT(&mn, DEBUG_FLAGS);
	BINARY_SOURCE(&mn, "ecas", "ecas.c");
	CONCAT(&mn, LDFLAGS);

	execute(&mn);
	
	return 0;
}
