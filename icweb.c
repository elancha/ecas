/* Copyright (C) 2023 Ernesto Lanchares <elancha98@gmail.com>
 * - All Rights Reserved
 * 
 * This file is part of ECAS.
 * 
 * ECAS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ECAS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

//TODO: Update Copyright
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#define ERROR(...) fprintf(stderr, "[ERROR]: " __VA_ARGS__)

#define SV(cstr_lit) ((sv) { \
		.len = sizeof(cstr_lit) - 1, \
		.data = (cstr_lit) \
	})
#define SV_Fmt "%.*s"
#define SV_Arg(sv) (int) (sv).len, (sv).data

#define LOC_Fmt "%s:%d:%d"
#define LOC_Arg(l) (l).file_path, (int)(l).row, (int)(l).col

typedef struct {
	char *data;
	size_t len;
} sv;

typedef struct {
	sv *items;
	size_t count;
	size_t capacity;
} sv_array;

typedef enum {
	TOKEN_WORD,
	TOKEN_TRIPLE_DASH,
	TOKEN_COLON,
	TOKEN_LESS,
	TOKEN_GREAT,
} token_type;

typedef struct {
	const char *file_path;
	size_t row, col;
} location;

#define MAX_KEYWORDS_CAPACITY 256
typedef struct {
	int c_like;
	char *keywords[MAX_KEYWORDS_CAPACITY];
	size_t keywords_count;
} language;

typedef struct {
	sv title;
} chapter;

typedef struct {
	chapter *items;
	size_t count;
	size_t capacity;
} chapter_array;

typedef struct {
	sv line;
	sv content;
	location loc;
} lexer;

#define MAX_AUTHORS_CAPACITY 256

typedef struct {
	sv title;
	sv authors[MAX_AUTHORS_CAPACITY];
	size_t author_count;
	sv license;
	language lang;
	sv purpose;
	sv abstract;
	chapter_array chapters;
} icweb;

static int
sv_eq(sv a, sv b) {
	if (a.len != b.len)
		return 0;

	for (size_t i = 0; i < a.len; i++) {
		if (a.data[i] != b.data[i])
			return 0;
	}
	return 1;
}

static int
sv_indexof(sv str, char c)
{
	for (size_t i = 0; i < str.len; i++) {
		if (str.data[i] == c)
			return i;
	}
	return -1;
}

static int
sv_startswith(sv str, sv prefix)
{
	if (str.len < prefix.len) {
		return 0;
	}

	for (size_t i = 0; i < prefix.len; i++) {
		if (str.data[i] != prefix.data[i])
			return 0;
	}
	return 1;
}

static int
sv_empty(sv str)
{
	for (size_t i = 0; i < str.len; i++) {
		if (!isspace(str.data[i]))
			return 0;
	}
	return 1;
}

static sv
sv_append(sv a, sv b)
{
	char *data = malloc(a.len + b.len);
	if (!data) {
		ERROR(" Out of memory\n");
		exit(1);
	}
	for (size_t i = 0; i < a.len; i++) {
		data[i] = a.data[i];
	}
	for (size_t i = 0; i < b.len; i++) {
		data[i + a.len] = b.data[i];
	}
	return (sv) {.data = data, .len = a.len + b.len};
}

static void
sv_trim_left(sv *a)
{
	while (a->len > 0 && isspace(a->data[0])) {
		a->data += 1;
		a->len -= 1;
	}
}

static char *
shift(int *argc, char ***argv)
{
	assert(*argc > 0);
	char *tmp = **argv;
	*argc -= 1;
	*argv += 1;
	return tmp;
}

static void
usage(FILE *stream, const char *program)
{
	fprintf(stream, "Usage: %s [OPTIONS] <input_file>\n", program);
	fprintf(stream, "OPTIONS:\n");
	fprintf(stream, "    -h    Print this message\n");
}

static sv
read_entire_file(const char *file_path)
{
	FILE *file = fopen(file_path, "r");
	if (!file) {
		ERROR("cannot open file %s: %s\n", file_path, strerror(errno));
		exit(1);
	}

	if (fseek(file, 0, SEEK_END) < 0)
		goto unknown_error;

	long m = ftell(file);
	if (m < 0)
		goto unknown_error;

	if (fseek(file, 0, SEEK_SET) < 0)
		goto unknown_error;

	char *buffer = malloc(sizeof(char)*m);
	if (buffer == 0)
		goto unknown_error;

	size_t n = fread(buffer, sizeof(char), m, file);
	if (ferror(file))
		goto unknown_error;

	fclose(file);

	return (sv){ .data = buffer, .len = n };

unknown_error:
	ERROR("Unknown error: %s\n", strerror(errno));
	exit(1);
}

static void
lexer_next_line(lexer *icweb_lexer)
{
	int l = sv_indexof(icweb_lexer->content, '\n');
	icweb_lexer->line.data = icweb_lexer->content.data;
	if (l < 0) {
		icweb_lexer->line.len = icweb_lexer->content.len;
		icweb_lexer->content.len = 0;
	}
	icweb_lexer->line.len = l;
	icweb_lexer->content.len -= l - 1;
	icweb_lexer->content.data += l + 1;
}

static sv
parse_flag(lexer *icweb_lexer)
{
	sv ret = {.data = icweb_lexer->line.data, .len = icweb_lexer->line.len};
	sv_trim_left(&ret);
	lexer_next_line(icweb_lexer);
	while (isspace(*icweb_lexer->line.data)) {
		sv_trim_left(&icweb_lexer->line);
		ret = sv_append(ret, icweb_lexer->line);
		lexer_next_line(icweb_lexer);
	}
	
	return ret;
}

static void
parse_authors(lexer *icweb_lexer, icweb *program)
{
	do {
		lexer_next_line(icweb_lexer);
		int l = sv_indexof(icweb_lexer->line, '-');
		if (l < 0)
			break;
		sv tmp = {.data = icweb_lexer->line.data, .len = l};
		if (!sv_empty(tmp))
			break;
		program->authors[program->author_count].data = icweb_lexer->line.data + l + 1;
		program->authors[program->author_count].len = icweb_lexer->line.len - l - 1;
		program->author_count += 1;
	} while (1);
}

static void
parse_header(lexer *icweb_lexer, icweb *program)
{
	while (!sv_startswith(icweb_lexer->line, SV("---"))) {
		int l = sv_indexof(icweb_lexer->line, ':');
		if (l < 0) {
			if (sv_empty(icweb_lexer->line)) {
				lexer_next_line(icweb_lexer);
			} else {
				fprintf(stderr, LOC_Fmt": ERROR: Expected flag but got `"SV_Fmt"`\n",
					LOC_Arg(icweb_lexer->loc),
					SV_Arg(icweb_lexer->line)
				);
				exit(1);
			}
		}
		sv flag = {.data = icweb_lexer->line.data, .len = l};
		icweb_lexer->loc.row += l+1;
		icweb_lexer->line.data += l+1;
		icweb_lexer->line.len -= l+1;

		if (sv_eq(flag, SV("Title"))) {
			program->title = parse_flag(icweb_lexer);
		} else if (sv_eq(flag, SV("Authors"))) {
			parse_authors(icweb_lexer, program);
		} else if (sv_eq(flag, SV("License"))) {
			program->license = parse_flag(icweb_lexer);
		} else if (sv_eq(flag, SV("Purpose"))) {
			program->purpose = parse_flag(icweb_lexer);
		} else if (sv_eq(flag, SV("Abstract"))) {
			program->abstract = parse_flag(icweb_lexer);
		} else if (sv_eq(flag, SV("Language"))) {
			/* TODO */
			parse_flag(icweb_lexer);
		} else {
			fprintf(stderr, LOC_Fmt": Unknown flag in prelude `"SV_Fmt"`\n",
				LOC_Arg(icweb_lexer->loc), SV_Arg(flag));
			exit(1);
		}
	}
}

static lexer
start_lexer(const char *file_path) {
	sv tmp = read_entire_file(file_path);
	int l = sv_indexof(tmp, '\n');
	if (l < 0) l = tmp.len;
	lexer tl = {0};
	tl.content.data = tmp.data + l;
	tl.content.len = tmp.len - l;
	tl.line.data = tmp.data;
	tl.line.len = l;
	tl.loc.file_path = file_path;
	tl.loc.row = 0;
	tl.loc.col = 0;
	return tl;
}

static void
print_icweb(icweb *program)
{
	if (program->title.len > 0)
		printf("Title: "SV_Fmt"\n", SV_Arg(program->title));
	if (program->author_count > 0)
		printf("Authors:\n");
	for (size_t i = 0; i < program->author_count; i++)
		printf(" - "SV_Fmt"\n", SV_Arg(program->authors[i]));
	if (program->license.len > 0)
		printf("License: "SV_Fmt"\n", SV_Arg(program->license));
	if (program->purpose.len > 0)
		printf("Purpose: "SV_Fmt"\n", SV_Arg(program->purpose));
	if (program->abstract.len > 0)
		printf("Abstract: "SV_Fmt"\n", SV_Arg(program->abstract));
}

// TODO:
//    - Support multiple files 

int
main(int argc, char **argv)
{
	const char *program = shift(&argc, &argv);
	const char *file_path = NULL;
	while (argc > 0) {
		const char *flag = shift(&argc, &argv);
		if (strcmp(flag, "-h") == 0) {
			usage(stdout, program);
			exit(0);
		} else {
			if (file_path) {
				ERROR("Multiple input files is not supported.\n");
				exit(1);
			}
			file_path = flag;
		}
	}

	if (!file_path) {
		ERROR("No input file is provided.\n");
		usage(stderr, program);
		exit(1);
	}

	icweb icweb_program = {0};
	lexer icweb_lexer = start_lexer(file_path);
	parse_header(&icweb_lexer, &icweb_program);

	// TOKENIZE

	// PARSE

	// TANGLE

	// PRETTY PRINT

	print_icweb(&icweb_program);

	return 0;
}
